/**
 * Team.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      regex: /^[a-z]{4,}$/
    },
    caption: {
      type: 'string'
    },
    phonenumber: {
      type: 'string',
      minLength: 10,
      maxLength: 10
    },
    password: {
      type: 'string',
      required: true,
      custom: function (value) {
        return _.isString(value) && value.length >= 6 && value.match(/[a-z]/i) && value.match(/[0-9]/);
      }      
    }
  },
};
