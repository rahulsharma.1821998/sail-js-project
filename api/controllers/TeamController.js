const jwt = require("jsonwebtoken");
const authUser = require("../policies/authUser");

module.exports = {
  addTeam: async function (req, res) {
    const authorization = req.headers["authorization"];
    const name = req.body.name;
    const caption = req.body.caption;
    const phonenumber = req.body.phonenumber;
    const password = req.body.password;

    const validateUser = authUser(authorization);
    if (validateUser == "jwt is expire") {
      res.send("jwt is expire");
    } else if (validateUser == "Authentication Fail") {
      res.send("authentication fail");
    } else {
      Team.create({ name, caption, phonenumber, password }, (err, result) => {
        if (err) {
          res.send(err);
        } else {
          if (result.rowCount > 0) {
            res.status(201).json({ msg: "successfully inserted" });
          } else {
            res.status(401).json({ msg: "try again" });
          }
        }
      });
      // const insertQuery =
      //   "INSERT INTO team (name, caption, phonenumber,  password) values ($1, $2, $3, $4)";
      // Team.query(
      //   insertQuery,
      //   [name, caption, phonenumber, password],
      //   (err, result) => {
      //     if (err) {
      //       res.send(err);
      //     } else {
      //       if(result.rowCount > 0){
      //         res.status(201).json({msg: "successfully inserted"});
      //       } else {
      //         res.status(401).json({msg: "try again"});
      //       }

      //     }
      //   }
      // );
    }
  },
  displayTeam(req, res) {
    const authorization = req.headers["authorization"];
    const validateUser = authUser(authorization);

    if (validateUser == "jwt is expire") {
      res.send("jwt is expire");
    } else if (validateUser == "Authentication Fail") {
      res.send("authentication fail");
    } else {
      const selectQuery = "select * from team";
      Team.query(selectQuery, (err, result) => {
        if (err) {
          res.send(err);
        } else {
          res.send(result.rows);
        }
      });
    }
  },
  displayOneTeam(req, res) {
    const id = req.params.id;
    const authorization = req.headers["authorization"];
    const validateUser = authUser(authorization);

    if (validateUser == "jwt is expire") {
      res.send("jwt is expire");
    } else if (validateUser == "Authentication Fail") {
      res.send("authentication fail");
    } else {
      const selectQuery = "select * from team where id = $1";
      Team.query(selectQuery, [id], (err, resultRows) => {
        if (err) {
          res.send(err);
        } else {
          res.send(resultRows.rows);
        }
      });
    }
  },
  deleteTeam(req, res) {
    const id = req.params.id;
    const authorization = req.headers["authorization"];
    const validateUser = authUser(authorization);
    if (validateUser == "jwt is expire") {
      res.send("jwt is expire");
    } else if (validateUser == "Authentication Fail") {
      res.send("authentication fail");
    } else {
      const deleteQuery = "DELETE FROM team WHERE id = $1";
      Team.query(deleteQuery, [id], (err, result) => {
        if (err) {
          res.send(err);
        } else {
          res.status(201).send("delete successfully");
        }
      });
    }
  },
  updateTeam(req, res) {
    const id = req.params.id;
    const name = req.body.name;
    const caption = req.body.caption;
    const phonenumber = req.body.phonenumber;
    const password = req.body.password;
    const authorization = req.headers["authorization"];
    const validateUser = authUser(authorization);

    if (validateUser == "jwt is expire") {
      res.send("jwt is expire");
    } else if (validateUser == "Authentication Fail") {
      res.send("authentication fail");
    } else {
      const updateQuery =
        "UPDATE team SET name = $2, caption = $3, phonenumber = $4,password = $5 WHERE id = $1";
      Team.query(
        updateQuery,
        [id, name, caption, phonenumber, password],
        (err, result) => {
          if (err) {
            res.send(err);
          } else {
            res.status(201).send("successfully update");
          }
        }
      );
    }
  },
};
