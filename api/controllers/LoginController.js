/**
 * LoginController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const jwt = require("jsonwebtoken");
module.exports = {
  loginTeam(req, res) {
    const name = req.body.name;
    const password = req.body.password;
    const secret = "secret";

    const selectQuery = "SELECT * FROM team WHERE name = $1 AND password = $2";
    Team.query(selectQuery, [name, password], (err, result) => {
      if (err) {
        console.log(err);
        res.send(err);
      } else {
        if (result.rowCount == 0) {
          res.send("try again");
        } else {
          const accessToken = jwt.sign(
            {
              id: result.rows[0].id,
              caption: result.rows[0].caption,
              password: result.rows[0].password,
              phonenumber: result.rows[0].phonenumber,
            },
            secret,
            {
              expiresIn: "30 days",
            }
          );

          res.json({ token: accessToken });
        }
      }
    });
  },
};
