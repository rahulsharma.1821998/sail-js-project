const { expect } = require("chai");
const supertest = require("supertest");

let token;
before(function (done) {
  let user = {
    name: "england",
    password: "rahul1",
  };
  supertest(sails.hooks.http.app)
    .post("/team/login")
    .send(user)
    .end((err, response) => {
      token = response.body.token;
      done();
    });
});

describe("addTeam", function () {
  it("addTeam", function (done) {
    const team = {
      name: "egnland",
      caption: "bain stokes",
      phonenumber: "1234567890",
      password: "india1",
    };
    const exprectResult = "successfully inserted";
    supertest(sails.hooks.http.app)
      .post("/team")
      .set({ authorization: `Bearer ${token}` })
      .send(team)
      .end(function (err, res) {
        expect(res.statusCode).to.equal(201);
        expect(res.text).to.equal(exprectResult);
      });
    done();
  });
  it("display team by id", function (done) {
    supertest(sails.hooks.http.app)
      .get("/team/1")
      .set({ authorization: `Bearer ${token}` })
      .end(function (err, res) {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.be.an('array');
      });
    done();
  });
  it("updateTeam", function (done) {
    const team = {
      name: "england",
      caption: "Betly",
      phonenumber: "1234567890",
      password: "india1",
    };
    const exprectResult = "successfully update";
    supertest(sails.hooks.http.app)
      .put("/team/1")
      .set({ authorization: `Bearer ${token}` })
      .send(team)
      .end(function (err, res) {
        expect(res.statusCode).to.equal(201);
        expect(res.text).to.equal(exprectResult);
      });
    done();
  });
  it("delete team by id", function (done) {
    const exprectResult = "delete successfully"
    supertest(sails.hooks.http.app)
      .delete("/team/8")
      .set({ authorization: `Bearer ${token}` })
      .end(function (err, res) {
        expect(res.statusCode).to.equal(201);
        expect(res.text).to.equal(exprectResult);
      });
    done();
  });
  it("display team", function (done) {
    supertest(sails.hooks.http.app)
      .get("/team")
      .set({ authorization: `Bearer ${token}` })
      .end(function (err, res) {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.be.an('array');
      });
    done();
  });
});
